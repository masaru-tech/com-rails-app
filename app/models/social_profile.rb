class SocialProfile < ApplicationRecord
  belongs_to :user

  def self.find_or_initialize_for_oauth(auth)
    profile = find_or_initialize_by(uid: auth["uid"], provider: auth["provider"]) do |pf|
      pf.name = auth["info"]["name"]
      pf.nickname = auth["info"]["nickname"]
      pf.image_url = auth["info"]["image"]
    end
    profile
  end
end
