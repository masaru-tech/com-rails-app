class Sake < ApplicationRecord
  acts_as_taggable

  validates :name, presence: true
  validates :yomi, presence: true

  def like(user)
    # 30分以内の評価は同一のものとみなす
    se = SakeEvaluation.where(sake_id: self.id, user_id: user.id, updated_at: (Time.now-30.minutes)..Time.now).order('id desc')
    if se.present?
      se.first.increment!(:evaluation)
    else
      SakeEvaluation.create!(sake_id: self.id, user_id: user.id, evaluation: 1)
    end
  end

  def dislike(user)
    # 30分以内の評価は同一のものとみなす
    se = SakeEvaluation.where(sake_id: self.id, user_id: user.id, updated_at: (Time.now-30.minutes)..Time.now).order('id desc')
    if se.present?
      se.first.decrement!(:evaluation)
    else
      SakeEvaluation.create!(sake_id: self.id, user_id: user.id, evaluation: -1)
    end
  end
end
