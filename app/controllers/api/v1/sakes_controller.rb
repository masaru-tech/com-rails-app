class Api::V1::SakesController < Api::ApplicationController
  def index
    if params[:keyword].blank?
      sakes = Sake.all
    else
      sakes = Sake.where('name like :keyword or yomi like :keyword_kata',
                {keyword: "%#{params[:keyword]}%", keyword_kata: "%#{Moji.hira_to_kata(params[:keyword])}%"})
    end

    sakes = paginate sakes.order(:id), page: params[:page], per_page: params[:per_page] || 20

    render json: sakes.map{|sake| Resources::V1::Sake.new(sake).as_json(only: [:id, :name, :yomi])}
  end

  def show
    render json: Resources::V1::Sake.new(Sake.find(params[:id]))
  end

  def like
    Sake.find(params[:id]).like(current_resource_owner)

    head :no_content
  end

  def dislike
    Sake.find(params[:id]).dislike(current_resource_owner)

    head :no_content
  end

  def evaluation
    se = SakeEvaluation.where(sake_id: params[:id], user_id: current_resource_owner.id, updated_at: (Time.now-30.minutes)..Time.now).first
    render json: {evaluation: se.present? ? se.evaluation : 0}
  end
end
