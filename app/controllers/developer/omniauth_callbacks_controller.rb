class Developer::OmniauthCallbacksController < ApplicationController
  include Monban::ControllerHelpers

  def twitter
    profile = SocialProfile.find_or_initialize_for_oauth(request.env['omniauth.auth'])
    user = profile.user

    unless user
      user = User.new(
        username: request.env['omniauth.auth']["info"]["name"]
      )
      user.save(validate: false)
      profile.user = user
      profile.save
    end

    sign_in(user)

    if session[:return_to]
      redirect_to session[:return_to]
      session[:return_to] = nil
    else
      redirect_to oauth_applications_path
    end
  end
end
