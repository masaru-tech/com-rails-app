class Developer::SessionsController < ApplicationController
  include Monban::ControllerHelpers

  def new
  end

  def destroy
    sign_out

    redirect_to developer_sessions_new_path
  end
end
