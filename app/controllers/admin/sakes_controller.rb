class Admin::SakesController < ApplicationController
  include Monban::ControllerHelpers
  layout 'doorkeeper/admin'

  def index
    @sakes = Sake.all.page(params[:page]).per(50)
  end

  def new
    @sake = Sake.new
  end

  def create
    sake = Sake.new(sake_params)
    sake.tag_list = params[:sake][:tag_list]
    if sake.save
      flash[:success] = "作成しました。"
      redirect_to edit_admin_sake_path(sake.id) and return
    else
      flash[:error] = "作成に失敗しました。"
    end

    redirect_to new_admin_sake_path
  end

  def edit
    @sake = Sake.find(params[:id])
  end

  def update
    sake = Sake.find(params[:id])

    if sake.update_attributes(sake_params)
      flash[:success] = "変更しました。"
    else
      flash[:error] = "変更に失敗しました。"
    end

    redirect_to edit_admin_sake_path(sake.id)
  end

  def tags
    render json: ActsAsTaggableOn::Tag.all.pluck(:name)
  end

  private
  def sake_params
    params.require(:sake).permit(:name, :yomi, :kuramoto, :locality, :raw_rice, :cleaning_rate,
      :nihonsyu_degree, :amino_degree, :alcohol_content, :acidit, :use_yeast, :tag_list)
  end
end
