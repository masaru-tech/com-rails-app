class Admin::SessionsController < ApplicationController
  include Monban::ControllerHelpers

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])

    if user && authenticate(user, params[:session][:password])
      sign_in(user)
      redirect_to oauth_applications_path
    else
      redirect_to admin_sessions_new_path
    end
  end

  def destroy
    sign_out

    redirect_to admin_sessions_new_path
  end
end
