Rails.application.routes.draw do
  use_doorkeeper

  namespace :api do
    namespace :v1 do
      get 'sakes' => 'sakes#index'
      get 'sakes/:id' => 'sakes#show'
      patch 'sakes/:id/like' => 'sakes#like'
      patch 'sakes/:id/dislike' => 'sakes#dislike'
      get 'sakes/:id/evaluation' => 'sakes#evaluation'
    end
  end

  namespace :developer do
    get 'sessions/new'
    delete 'sessions' => 'sessions#destroy'
  end

  namespace :admin do
    get 'sessions/new'
    post 'sessions' => 'sessions#create'
    delete 'sessions' => 'sessions#destroy'

    resources :sakes do
      collection do
        get 'tags'
      end
    end
  end

  scope module: :developer do
    [:twitter].each do |provider|
      get "auth/#{provider}/callback" => "omniauth_callbacks##{provider}"
    end
  end

  root to: 'home#index'

end
