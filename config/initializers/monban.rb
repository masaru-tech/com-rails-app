Monban.configure do |config|
  config.user_lookup_field = :username
  config.no_login_redirect = {controller: 'developer/sessions', action: 'new'}
end
